#!/usr/bin/env node
const chalk = require('chalk');
const figlet = require('figlet');

console.log(
    chalk.magenta(
        figlet.textSync('Clawless CLI', { horizontalLayout: 'full' })
    )
);